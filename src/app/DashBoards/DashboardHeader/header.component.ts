import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Auth/UserModel';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/Auth/Services/AuthenicatonServices';

@Component({
  selector: 'app-dashboardheader',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class DashBoardHeaderComponent {

  currentUser: User;

  constructor(
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }
}
