import { Component, OnInit } from '@angular/core';
import {HttpClient} from  '@angular/common/http';
import { first } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardHomeComponent implements OnInit {
  images = [];
  cancelIcon='src/assets/cancel.png';


  constructor(private http:HttpClient) {
    if (this.images.length==0)
    {
 
      this.images=(JSON.parse( localStorage.getItem('SliderImages')));

    }
   
   }
   

  ngOnInit() {
    }
  

  onFileSelected(event){
   var name=event.target.files[0].name;
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'png' || ext.toLowerCase()== 'jpg'|| ext.toLowerCase()== 'jpeg'  || ext.toLowerCase()== 'bmp'){
        
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();

              reader.onload = (event:any) => {
                var value=event.target.result;
                 this.images.push(event.target.result); 
                
              }

              reader.readAsDataURL(event.target.files[i]);
      }
     }
  }
  else
  {
   alert('Please select a valid image') ;
  
     }
  }


  Submit(index) {
  
    localStorage.setItem("SliderImages",JSON.stringify( this.images));
     
alert('Upload successfuly Thanks !')

  }
  cancel(index){
   
   this.images.splice(index,1);
   


  }
  
 
  
  
}

    
  


