import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-portfolio',
  templateUrl: './dashboard-portfolio.component.html',
  styleUrls: ['./dashboard-portfolio.component.css']
})
export class DashboardPortfolioComponent implements OnInit {
file;
Text;
bannerImage1;
bannerImage2;
bannerImage3;
bannerImage4;
bannerImage5;
bannerImage6;
imagePreview1;
imagePreview2;
imagePreview3;
imagePreview4;
imagePreview5;
imagePreview6;
image;
  constructor() {
   this.imagePreview1= localStorage.getItem('imagePreview1');
    this.imagePreview2= localStorage.getItem('imagePreview2');
    this.imagePreview3= localStorage.getItem('imagePreview3');
    this.imagePreview4= localStorage.getItem('imagePreview4');
    this.imagePreview5 =localStorage.getItem('imagePreview5');
   this.imagePreview6= localStorage.getItem('imagePreview6');
    this.Text= localStorage.getItem('PortfolioText');
    
   }
  onFileSelected(event){
    var name=event.target.files[0].name;
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'png' || ext.toLowerCase()== 'jpg'  || ext.toLowerCase()== 'jpeg'  || ext.toLowerCase()== 'bmp'){
  this.file= event.target.files[0]
  var attribute=event.target;
  var id=attribute.id;
 
  const reader = new FileReader();
  reader.onload = () => {
  var imagePreview = reader.result;
  
  console.log(id);
  switch(id){
   
    case 'bannerImage1':{
      this.imagePreview1=imagePreview;
      break;
   
    }
    case 'bannerImage2':{
      this.imagePreview2= imagePreview;
      break;
      }
      case 'bannerImage3':{
        this.imagePreview3=imagePreview;
        break;
        }
        case 'bannerImage4':{
          this.imagePreview4= imagePreview;
          break;
    
          }
          case 'bannerImage5':{
            this.imagePreview5 =imagePreview;
            break;
            }
            default :{
              this.imagePreview6= imagePreview;
              break;
              }
    
     }};
 this.image= reader.readAsDataURL(this.file);
    }
    else{
      alert('Please select a valid image') ;

    }
}

Submit(){
  localStorage.setItem('imagePreview1', this.imagePreview1);
  localStorage.setItem('imagePreview2', this.imagePreview2);
  localStorage.setItem('imagePreview3', this.imagePreview3);
  localStorage.setItem('imagePreview4', this.imagePreview4);
  localStorage.setItem('imagePreview5', this.imagePreview5);
  localStorage.setItem('imagePreview6', this.imagePreview6);
  localStorage.setItem('PortfolioText', this.Text);
  
  alert('Upload successfuly Thanks !')
  
    }
  
  ngOnInit() {
  }

}
