import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-services',
  templateUrl: './dashboard-services.component.html',
  styleUrls: ['./dashboard-services.component.css']
})
export class DashboardServicesComponent implements OnInit {

  Text:string;
  Text1;
  Text2;
  Text3;
  Text4;
  Text5;
  Text6;
  HeadingText1;
  HeadingText2;
  HeadingText3;
  HeadingText4;
  HeadingText5;
  HeadingText6;
  constructor() { 
  this.Text = localStorage.getItem('ServicesText');
  this.Text1 = localStorage.getItem('ServicesText1');
  this.Text2 = localStorage.getItem('ServicesText2');
  this.Text3 = localStorage.getItem('ServicesText3');
  this.Text4 = localStorage.getItem('ServicesText4');
  this.Text5 = localStorage.getItem('ServicesText5');
  this.Text6 = localStorage.getItem('ServicesText6');
  this. HeadingText1 = localStorage.getItem('ServicesHeadingText1');
  this. HeadingText2 = localStorage.getItem('ServicesHeadingText2');
  this. HeadingText3 = localStorage.getItem('ServicesHeadingText3');
  this. HeadingText4 = localStorage.getItem('ServicesHeadingText4');
  this. HeadingText5 = localStorage.getItem('ServicesHeadingText5');
  this. HeadingText6 = localStorage.getItem('ServicesHeadingText6');
  
  }

  ngOnInit() {
  }
  Submit(){
localStorage.setItem('ServicesText', this.Text);
localStorage.setItem('ServicesText1', this.Text1);
localStorage.setItem('ServicesText2', this.Text2);
localStorage.setItem('ServicesText3', this.Text3);
localStorage.setItem('ServicesText4', this.Text4);
localStorage.setItem('ServicesText5', this.Text5);
localStorage.setItem('ServicesText6', this.Text6);
localStorage.setItem('ServicesHeadingText1', this.HeadingText1);
localStorage.setItem('ServicesHeadingText2', this.HeadingText2);
localStorage.setItem('ServicesHeadingText3', this.HeadingText3);
localStorage.setItem('ServicesHeadingText4', this.HeadingText4);
localStorage.setItem('ServicesHeadingText5', this.HeadingText5);
localStorage.setItem('ServicesHeadingText6', this.HeadingText6);

alert('Upload successfuly Thanks !')

  }

}
