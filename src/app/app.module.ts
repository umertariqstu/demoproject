import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SliderComponent } from './slider/slider.component';
import {SlideshowModule} from 'ng-simple-slideshow';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DashboardHomeComponent } from './DashBoards/DashboardHome/dashboard.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {  ReactiveFormsModule } from '@angular/forms';
import {  ErrorInterceptor } from '../app/Auth/Intercepter/ErrorInterceptor';
import {JwtInterceptor} from '../app/Auth/Intercepter/JWTInterceptor';
import {fakeBackendProvider} from '../app/Auth/FackBackendInterceptor';
import {DashBoardHeaderComponent} from '../app/DashBoards/DashboardHeader/header.component';
import { DashboardAboutUsComponent } from './DashBoards/Dashboard-about-us/dashboard-about-us.component';
 
import { DashboardServicesComponent } from './DashBoards/dashboard-services/dashboard-services.component';
import { ServicesComponent } from './services/services.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AboutUsComponent } from './about-us/about-us.component';
import {DashboardPortfolioComponent} from '../app/DashBoards/Dashboard-portfolio/dashboard-portfolio.component';
import {NgsRevealModule} from 'ngx-scrollreveal';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SliderComponent,
    DashboardHomeComponent,
    LoginComponent,
    DashBoardHeaderComponent,
    DashboardAboutUsComponent,
    DashboardServicesComponent,
    ServicesComponent,
    PortfolioComponent,
    AboutUsComponent,
    DashboardPortfolioComponent,
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlideshowModule,
    AngularFontAwesomeModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgsRevealModule,
    
    
    
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
