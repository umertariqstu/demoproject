import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from '../app/home/home.component';
import {DashboardHomeComponent} from './DashBoards/DashboardHome/dashboard.component';
import {AuthGuard} from '../app/Auth/AuthGuard';
import {LoginComponent} from '../app/login/login.component';
import {DashboardAboutUsComponent} from '../app/DashBoards/Dashboard-about-us/dashboard-about-us.component';
import {DashboardPortfolioComponent} from '../app/DashBoards/Dashboard-portfolio/dashboard-portfolio.component';
import {DashboardServicesComponent} from '../app/DashBoards/dashboard-services/dashboard-services.component';
const routes: Routes = [
{path:'' , component:HomeComponent },
{ path: 'login', component: LoginComponent },
{ path: 'dashboard', component: DashboardHomeComponent, canActivate: [AuthGuard]  },
{ path: 'AboutUs', component: DashboardAboutUsComponent, canActivate: [AuthGuard]  },
{ path: 'services', component: DashboardServicesComponent, canActivate: [AuthGuard]  },
{ path: 'portfolio', component: DashboardPortfolioComponent, canActivate: [AuthGuard]  },
    // otherwise redirect to home
     { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
