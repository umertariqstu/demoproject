import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
Text;
imagePreview1;
imagePreview2;
imagePreview3;
imagePreview4;
imagePreview5;
imagePreview6;
  constructor() { 

    this.imagePreview1= localStorage.getItem('imagePreview1');
    this.imagePreview2= localStorage.getItem('imagePreview2');
    this.imagePreview3= localStorage.getItem('imagePreview3');
    this.imagePreview4= localStorage.getItem('imagePreview4');
    this.imagePreview5 =localStorage.getItem('imagePreview5');
   this.imagePreview6= localStorage.getItem('imagePreview6');
    this.Text= localStorage.getItem('PortfolioText');
    

  }

  ngOnInit() {
  }

}
